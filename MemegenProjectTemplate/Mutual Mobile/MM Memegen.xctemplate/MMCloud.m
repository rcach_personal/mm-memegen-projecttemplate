//
//  MMCloud.m
//  MMMemegen
//
//  Created by Josh Berlin on 12/10/12.
//  Copyright (c) 2012 Mutual Mobile. All rights reserved.
//

#import "MMCloud.h"

#import <Parse/Parse.h>

@implementation MMCloud

+ (void)beginCollectingCondensationWithUsername:(NSString *)username
                                       andEmail:(NSString *)email {
  [Parse setApplicationId:@"JLeugHIeavTnEoGoU5X6bDeBDZhjn0NUTFLGyS5t"
                clientKey:@"ZBqgdsuoQM159hTpseoM1SjuPZMvUkNF0mFQDPzL"];
  
  // Wipe out old user defaults
  if ([[NSUserDefaults standardUserDefaults] objectForKey:@"objectIDArray"]){
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"objectIDArray"];
  }
  [[NSUserDefaults standardUserDefaults] synchronize];
  
  // Simple way to create a user or log in the existing user
  // For your app, you will probably want to present your own login screen
  PFUser *currentUser = [PFUser currentUser];
  
  if (!currentUser) {
    // Dummy username and password
    PFUser *user = [PFUser user];
    user.username = username;
    user.password = @"password";
    user.email = email;
    
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
      if (error) {
        // Assume the error is because the user already existed.
        [PFUser logInWithUsername:@"Matt" password:@"password"];
      }
    }];
  }
  PFACL *defaultACL = [PFACL ACL];
  [defaultACL setPublicReadAccess:YES];
  [PFACL setDefaultACL:defaultACL withAccessForCurrentUser:YES];
}

+ (void)uploadImage:(UIImage *)image onComplete:(void (^)())onComplete {
  NSData *imageData = UIImageJPEGRepresentation(image, 0.60f);
  [self uploadImageData:imageData onComplete:onComplete];
}

+ (void)uploadImageData:(NSData *)imageData
             onComplete:(void (^)())onComplete {
  PFFile *imageFile = [PFFile fileWithName:@"Image.jpg" data:imageData];
  
  [imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
    if (!error) {
      PFObject *userPhoto = [PFObject objectWithClassName:@"UserPhoto"];
      [userPhoto setObject:imageFile forKey:@"imageFile"];
      
      userPhoto.ACL = [PFACL ACLWithUser:[PFUser currentUser]];
      
      PFUser *user = [PFUser currentUser];
      [userPhoto setObject:user forKey:@"user"];
      
      [userPhoto saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
        if (!error) {
          onComplete();
        } else {
          NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
      }];
    } else {
      NSLog(@"Error: %@ %@", error, [error userInfo]);
    }
  } progressBlock:^(int percentDone) {
    
  }];
}

@end
