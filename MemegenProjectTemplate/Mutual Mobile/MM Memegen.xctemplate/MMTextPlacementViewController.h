//
//  MMTextPlacementViewController.h
//  ___PACKAGENAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//

@interface MMTextPlacementViewController : UIViewController
@property(nonatomic, strong) UIImage *memeImage;
@property(nonatomic, strong) NSString *memeText;
@property (weak, nonatomic) IBOutlet UILabel *memeLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *memeLabelHeightConstraint;
@end
