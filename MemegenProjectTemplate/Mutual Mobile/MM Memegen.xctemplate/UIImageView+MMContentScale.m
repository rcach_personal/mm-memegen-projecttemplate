//
//  UIImageView+MMContentScale.m
//  MMMemegen
//
//  Created by Rene Cacheaux on 11/14/12.
//  Copyright (c) 2012 Mutual Mobile. All rights reserved.
//

#import "UIImageView+MMContentScale.h"

@implementation UIImageView (MMContentScale)

-(CGFloat)contentScaleFactor
{
  CGFloat widthScale = self.bounds.size.width / self.image.size.width;
  CGFloat heightScale = self.bounds.size.height / self.image.size.height;
  
  if (self.contentMode == UIViewContentModeScaleToFill) {
    return (widthScale==heightScale) ? widthScale : NAN;
  }
  if (self.contentMode == UIViewContentModeScaleAspectFit) {
    return MIN(widthScale, heightScale);
  }
  if (self.contentMode == UIViewContentModeScaleAspectFill) {
    return MAX(widthScale, heightScale);
  }
  return 1.0;
  
}

@end
