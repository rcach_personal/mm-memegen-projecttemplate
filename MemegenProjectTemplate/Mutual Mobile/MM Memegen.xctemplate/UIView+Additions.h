//
//  UIView+Additions.h
//  MMMemegen
//
//  Created by Josh Berlin on 1/4/13.
//  Copyright (c) 2013 Mutual Mobile. All rights reserved.
//

@interface UIView (MMAdditions)

- (UIImage *)takeScreenshot;

@end
